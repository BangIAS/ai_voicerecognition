const btn = document.querySelector('.bicara');
const content = document.querySelector('.content');

//Data Balasan
const greetings = [
    'Apik-apik wae gaes',
    'Sangat santuy',
    'Mantap Jiwa',
    'Jos Gandos',
    'Sehat dong broooo!!'
];

const weather = [
    'Cerah banget bro!',
    'Tidak cerah',
    'Mendung ndes!',
    'Mendung, harap di rumah saja jika anda jomblo'
];

const health = [
    'Aku terkena virus',
    'ada sedikit virus cinta',
    'tidak baik sekali diriku bro'
];

const laugh = [
    'hahahaaaa',
    'hihihii',
    'waka waka waka waka kakakakakaaaaa'
];

const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
const recognition = new SpeechRecognition();

recognition.onstart = function () {
    console.log('voice is actived, you can to microphone');
};

recognition.onresult = function (event) {
    const current = event.resultIndex;

    const transcript = event.results[current][0].transcript;
    content.textContent = transcript;
    readOutLoud(transcript);
}

//add the listener to the btn

btn.addEventListener('click', () =>{
    recognition.start();
});

function readOutLoud(message) {
    const speech = new SpeechSynthesisUtterance();

    speech.text = 'Maaf, saya tidak tahu apa yang anda bicarakan';

    if(message.includes('apa kabar')){
        const finalText = 
            greetings[Math.floor(Math.random()*greetings.length)];
        speech.text = finalText;
    }

    else if(message.includes('cuaca')){
        const finalText = 
            weather[Math.floor(Math.random()*weather.length)];
        speech.text = finalText;
    }

    else if(message.includes('kondisi')){
        const finalText = 
            health[Math.floor(Math.random()*health.length)];
        speech.text = finalText;
    }

    else if(message.includes('tertawa')){
        const finalText = 
            laugh[Math.floor(Math.random()*laugh.length)];
        speech.text = finalText;
    }

    speech.volume = 1;
    speech.rate = 1;
    speech.pitch = 1;

    window.speechSynthesis.speak(speech);
}